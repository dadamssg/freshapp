package com.fresh.app.schemas;


import com.programmingarehard.fresh.SQL.Schema;

public class ProfileSchema extends Schema {

    @Override
    public String table() {
        return "profiles";
    }

    @Override
    public void build() {

        primaryColumn("_id");
        column("created_at", LONG);
        column("updated_at", LONG);
        column("likes", TEXT).defaultTo("");
        column("dislikes", TEXT).defaultTo("");
    }
}
