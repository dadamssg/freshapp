package com.fresh.app.schemas;

import com.programmingarehard.fresh.SQL.Schema;

public class GroupRelationships extends Schema {

    @Override
    public String table() {
        return "groups_users";
    }

    @Override
    public void build() {

        primaryColumn("_id");
        column("user_id", INTEGER).defaultTo(0);
        column("group_id", INTEGER).defaultTo(0);
    }
}
