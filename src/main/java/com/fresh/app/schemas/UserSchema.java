package com.fresh.app.schemas;


import com.programmingarehard.fresh.SQL.Schema;

public class UserSchema extends Schema {

    @Override
    public String table() {
        return "users";
    }

    @Override
    public void build() {

        primaryColumn("_id");
        column("created_at", LONG);
        column("updated_at", LONG);
        column("name", TEXT).defaultTo("");
        column("description", TEXT).defaultTo("");
        column("profile_id", INTEGER).defaultTo(0);
    }
}
