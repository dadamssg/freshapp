package com.fresh.app.schemas;

import com.programmingarehard.fresh.SQL.Schema;

public class GroupSchema extends Schema {

    @Override
    public String table() {
        return "groups";
    }

    @Override
    public void build() {

        primaryColumn("_id");
        column("name", TEXT).defaultTo("");
    }
}
