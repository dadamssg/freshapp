package com.fresh.app.schemas;

import com.programmingarehard.fresh.SQL.Schema;

public class NoteSchema extends Schema {

    @Override
    public String table() {
        return "notes";
    }

    @Override
    public void build() {

        primaryColumn("_id");
        column("created_at", LONG);
        column("updated_at", LONG);
        column("user_id", INTEGER).defaultTo(0);
        column("contents", TEXT).defaultTo("");
    }
}
