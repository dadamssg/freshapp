package com.fresh.app;

import android.content.Context;
import com.fresh.app.schemas.*;
import com.programmingarehard.fresh.Fresh;

/**
 * Created with IntelliJ IDEA.
 * User: davidadams
 * Date: 8/26/13
 * Time: 5:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class FreshORM extends Fresh {

    public FreshORM(Context context) {

        super(context);

        registerSchema(GroupRelationships.class);
        registerSchema(GroupSchema.class);
        registerSchema(NoteSchema.class);
        registerSchema(ProfileSchema.class);
        registerSchema(UserSchema.class);
    }
}
