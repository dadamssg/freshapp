package com.fresh.app.models;

import com.fresh.app.schemas.UserSchema;
import com.programmingarehard.fresh.Model;
import com.programmingarehard.fresh.SQL.Schema;
import com.programmingarehard.fresh.relationships.BelongsToMany;
import com.programmingarehard.fresh.relationships.HasMany;
import com.programmingarehard.fresh.relationships.HasOne;


public class User extends Model {

    public HasMany<Note> notes(){

        return hasMany(Note.class);
    }

    public HasOne<Profile> profile(){

        return hasOne(Profile.class);
    }

    public BelongsToMany<Group> groups(){

        return belongsToMany(Group.class);
    }

    @Override
    public Class<? extends Schema> schema(){

        return UserSchema.class;
    }

    @Override
    public String toString(){

        return get("name");
    }

}
