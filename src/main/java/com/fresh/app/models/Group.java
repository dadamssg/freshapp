package com.fresh.app.models;

import com.fresh.app.schemas.GroupSchema;
import com.programmingarehard.fresh.Model;
import com.programmingarehard.fresh.SQL.Schema;
import com.programmingarehard.fresh.relationships.BelongsToMany;

public class Group extends Model {

    public Group() {
        hasTimestamps = false;
    }

    public BelongsToMany<User> users(){

        return belongsToMany(User.class);
    }

    @Override
    public Class<? extends Schema> schema(){

        return GroupSchema.class;
    }

    @Override
    public String toString(){

        return get("name");
    }

}
