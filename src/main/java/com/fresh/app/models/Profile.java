package com.fresh.app.models;

import com.fresh.app.schemas.ProfileSchema;
import com.fresh.app.schemas.UserSchema;
import com.google.inject.Inject;
import com.programmingarehard.fresh.Model;
import com.programmingarehard.fresh.SQL.Schema;
import com.programmingarehard.fresh.relationships.BelongsTo;


public class Profile extends Model {

    public BelongsTo<User> user(){

        return belongsTo(User.class);
    }

    @Override
    public Class<? extends Schema> schema(){

        return ProfileSchema.class;
    }

    @Override
    public String toString(){

        String profile = "Likes: " + get("likes") + ".\nDislikes: " + get("dislikes");

        return profile;
    }
}
