package com.fresh.app.models;

import com.fresh.app.schemas.NoteSchema;
import com.fresh.app.schemas.UserSchema;
import com.programmingarehard.fresh.Model;
import com.programmingarehard.fresh.SQL.Schema;
import com.programmingarehard.fresh.relationships.HasOne;


public class Note extends Model {

    public HasOne<User> user(){

        return hasOne(User.class);
    }

    @Override
    public Class<? extends Schema> schema(){

        return NoteSchema.class;
    }

    @Override
    public String toString(){

        return get("contents");
    }
}
