package com.fresh.activities;

import android.os.AsyncTask;
import android.widget.TextView;
import com.fresh.app.FreshORM;
import com.fresh.app.models.Group;
import com.fresh.app.models.Note;
import com.fresh.app.models.Profile;
import com.fresh.app.models.User;
import com.google.inject.Inject;
import com.fresh.R;
import android.os.Bundle;
import com.programmingarehard.fresh.DataSource;
import com.programmingarehard.fresh.Fresh;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

import java.util.List;

public class HomeActivity extends RoboActivity {

    private final static String DBG = "FreshORM";

    private DataSource dataSource;

    private Fresh app;

    @InjectView(R.id.helloWorld)
    TextView textView;

    String resultString = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_layout);

        app = new FreshORM(this);

        dataSource = app.getInstance(DataSource.class);

        new Demonstration().execute((Void) null);
    }

    public void demonstrateSaves() {

        //First let's create some Users
        User David = app.getInstance(User.class);
        David.set("name", "David Adams");
        David.set("description", "wannabe Android developer");
        David.save();

        User Kara = app.getInstance(User.class);

        //if you prefer a more expressive syntax you can do the following
        Kara.set("name").to("Kara")
            .set("description").to("David's girlfriend")
        .save();

        //Now let's create a profile for David
        Profile DavidsProfile = app.getInstance(Profile.class);
        DavidsProfile.set("likes", "fun");
        DavidsProfile.set("dislikes", "getting punched in the face");

        /*
            Explicit saves are unnecessary if the Model attaches,
            or gets attached, to another Model. The Relationship
            will save both if need be.
        */

        //And attach the profile to a User
        Boolean wasSuccessful = David.profile().attach(DavidsProfile);

        //create some notes Models
        Note note1 = app.getInstance(Note.class);
        note1.set("contents", "blah blah blah");

        Note note2 = app.getInstance(Note.class);
        note2.set("contents", "some interesting stuff");

        //attach them to David in different but equal ways
        David.notes().attach(note1);
        note2.user().attach(David);

        //detach note1 for show
        David.notes().detach(note1);

        //let's create a "human" group
        Group humanGroup = app.getInstance(Group.class);
        humanGroup.set("name", "humans");

        //let's attach a User to a Group
        humanGroup.users().attach(David);

        //and let's go the other way and attach a Group to a User
        Kara.groups().attach(humanGroup);

        //let's create a Men group
        Group men = app.getInstance(Group.class);
        men.set("name", "men");

        //and attach it to David
        men.users().attach(David);
    }

    public void demonstrateResultsofSaves() {

        User David = dataSource.getModelById(1, User.class);

        Profile DavidsProfile = David.profile().get();

        List<Group> DavidsGroups = David.groups().get();

        User Kara = dataSource.getModelById(2, User.class);

        List<Group> KarasGroups = Kara.groups().get();

        List<Note> DavidsNotes = David.notes().get();

        StringBuilder sb = new StringBuilder();

        sb.append("User: ");
        sb.append(David);
        sb.append("\n David's profile: ");

        if (DavidsProfile != null) {
            sb.append(DavidsProfile);
        }
        sb.append("\n David's groups: ");

        for (Group group : DavidsGroups) {
            sb.append("\n");
            sb.append(group);
        }

        sb.append("\n David's notes: ");

        for (Note note : DavidsNotes) {
            sb.append("\n");
            sb.append(note);
        }

        sb.append("\n\n");
        sb.append("User: ");
        sb.append(Kara);

        for (Group group : KarasGroups) {
            sb.append("\n");
            sb.append(group);
        }

        resultString = sb.toString();
    }

    private class Demonstration extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            Boolean wasSuccessful = false;

            try {

                demonstrateSaves();

                demonstrateResultsofSaves();

                wasSuccessful = true;

            } catch (Exception e) {
                e.printStackTrace();
            }

            return wasSuccessful;
        }

        @Override
        protected void onPostExecute(Boolean wasSuccessful) {

            if (wasSuccessful) {
                textView.setText(resultString);
            } else {
                textView.setText("An error occured :(");
            }
        }
    }
}